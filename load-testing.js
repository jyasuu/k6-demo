import { check } from 'k6';
import http from 'k6/http';

export default function () {
  // 從 ENV 中取得變數值
  const url = `${__ENV.TESTING_URL}`;
  const check_keyword = `${__ENV.TESTING_KEYWORD}`;

  const result = http.get(url);
  check(result, {
    'http code = 200': (r) => r.status === 200,
    'verify homepage text': (r) => r.body.includes(check_keyword),
  });
}

export const options = {
// duration 與 vus 改由 ENV 取得，因此將下面兩行註解
//   duration: '1m',
//   vus: 10,
  thresholds: {
    http_req_duration: ['p(95)<500'], 
  },
};